
	<!-- Start Footer Area -->
	<footer class="footer">
		<!-- Footer Top -->
		<div class="footer-top section">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
							<div class="logo">
							<a href="{{route('home')}}">
								<h2 style="color:#ccc;"> Jual<span style="color:#F7941D">Emas</span></h2>
							</a>
							</div>
							@php
								$settings=DB::table('settings')->get();
							@endphp
							<p class="text">@foreach($settings as $data) {{$data->short_des}} @endforeach</p>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-2 col-md-6 col-12">

						<!-- End Single Widget -->
					</div>
					<div class="col-lg-2 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Layanan Penjualan</h4>
							<ul>
								<li><a href="#">Pembelian Cash</a></li>
								<li><a href="#">Pembelian Cicilan</a></li>
								<li><a href="#">CashBack</a></li>
								<li><a href="#">Pengiriman</a></li>
								<li><a href="#">Kebijakan Privasi</a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Informasi</h4>
							<ul>
								<li><a href="{{route('about-us')}}">Tentang Jual<span style="color:#F7941D">Emas</span></a></li>
								<li><a href="#">Faq</a></li>
								<li><a href="#">Syarat dan Ketentuan</a></li>
								<li><a href="{{route('contact')}}">Hubungi Kami</a></li>
								<li><a href="#">Bantuan</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-lg-6 col-12">
							<div class="left">
								<p>Copyright © {{date('Y')}} <a href="https://github.com/wartatv" target="_blank">WartaTV</a>  -  All Rights Reserved.</p>
							</div>
						</div>
						<div class="col-lg-6 col-12">

						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /End Footer Area -->
 
	<!-- Jquery -->
    <script src="{{asset('front/js/jquery.min.js')}}"></script>
    <script src="{{asset('front/js/jquery-migrate-3.0.0.js')}}"></script>
	<script src="{{asset('front/js/jquery-ui.min.js')}}"></script>
	<!-- Popper JS -->
	<script src="{{asset('front/js/popper.min.js')}}"></script>
	<!-- Bootstrap JS -->
	<script src="{{asset('front/js/bootstrap.min.js')}}"></script>
	<!-- Color JS -->
	<script src="{{asset('front/js/colors.js')}}"></script>
	<!-- Slicknav JS -->
	<script src="{{asset('front/js/slicknav.min.js')}}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{asset('front/js/owl-carousel.js')}}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{asset('front/js/magnific-popup.js')}}"></script>
	<!-- Waypoints JS -->
	<script src="{{asset('front/js/waypoints.min.js')}}"></script>
	<!-- Countdown JS -->
	<script src="{{asset('front/js/finalcountdown.min.js')}}"></script>
	<!-- Nice Select JS -->
	<script src="{{asset('front/js/nicesellect.js')}}"></script>
	<!-- Flex Slider JS -->
	<script src="{{asset('front/js/flex-slider.js')}}"></script>
	<!-- ScrollUp JS -->
	<script src="{{asset('front/js/scrollup.js')}}"></script>
	<!-- Onepage Nav JS -->
	<script src="{{asset('front/js/onepage-nav.min.js')}}"></script>
	{{-- Isotope --}}
	<script src="{{asset('front/js/isotope/isotope.pkgd.min.js')}}"></script>
	<!-- Easing JS -->
	<script src="{{asset('front/js/easing.js')}}"></script>

	<!-- Active JS -->
	<script src="{{asset('front/js/active.js')}}"></script>

	
	@stack('scripts')
	<script>
		setTimeout(function(){
		  $('.alert').slideUp();
		},5000);
		$(function() {
		// ------------------------------------------------------- //
		// Multi Level dropdowns
		// ------------------------------------------------------ //
			$("ul.dropdown-menu [data-toggle='dropdown']").on("click", function(event) {
				event.preventDefault();
				event.stopPropagation();

				$(this).siblings().toggleClass("show");


				if (!$(this).next().hasClass('show')) {
				$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
				}
				$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
				$('.dropdown-submenu .show').removeClass("show");
				});

			});
		});
	  </script>